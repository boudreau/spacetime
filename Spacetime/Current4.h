#ifndef _Current4_h_
#define _Current4_h_
#include <Spacetime/DiracSpinor.h>
#include <Spacetime/Operator4.h>
#include <Spacetime/FourVector.h>
#include <memory>
// ======================================================//
//                                                       //
// This class is to be used completely behind the scenes //
// and facilitiates the following operation:             //
//                                                       //
// FourVector jL=uBar*Gamma()*u;                         //
//                                                       //
// ======================================================//
class Current4 {

 public:
  
  operator ComplexFourVector() const;
  
 private:

  std::shared_ptr<const DiracSpinor::Any> spinor;
  std::shared_ptr<const DiracSpinor::Bar> spinorBar;
  std::shared_ptr<const AbsOperator4>     op;
  mutable std::shared_ptr<ComplexFourVector>      current;
  friend Current4 operator * (const DiracSpinor::Bar &, const AbsOperator4 &);
  friend Current4 operator * (const Current4         &, const DiracSpinor::Any &    );
};

Current4 operator * (const DiracSpinor::Bar &, const AbsOperator4 &);
Current4 operator * (const Current4         &, const DiracSpinor::Any &    );

#endif
