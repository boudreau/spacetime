#ifndef _ThreeVector_h_
#define _ThreeVector_h_
#include "Spacetime/ThreeVector.h"
#include <iostream>
#include <complex>
class Rotation;


template <typename T>
class BasicThreeVector {

 public:

  // Construct a zero vector
  BasicThreeVector();

  // Construct from components
  BasicThreeVector(T x0, T x1, T x2);

  // Access to elements:
  const T & operator[] (unsigned int i) const;
  T & operator[] (unsigned int i);

  // Access to elements: 
  const T & operator() (unsigned int i) const;
  T & operator() (unsigned int i);

  // Cross and dot. (No automatic complex conjugation of first operand)
  BasicThreeVector<T> cross(const BasicThreeVector<T> & right) const;
  T dot(const BasicThreeVector<T> & source) const;

  // Compound operations
  BasicThreeVector<T> & operator +=(const BasicThreeVector<T> & right);
  BasicThreeVector<T> & operator -=(const BasicThreeVector<T> & right);
  BasicThreeVector<T> & operator *=(T s);
  BasicThreeVector<T> & operator /=(T s);

  // Unary minus:
  BasicThreeVector<T> operator- () const;

  // Conjugate
  BasicThreeVector<T> conjugate() const;
  
  // Norm:
  double norm() const;

  // Squared norm:
  double squaredNorm() const;

  // Return normalized version
  BasicThreeVector<T> normalized() const;
  
  
 private:

  // Store the components of this vector;
  T x[3];
  
};


// Other operations:
template <typename T> std::ostream & operator << (std::ostream & o, const BasicThreeVector<T> &v);
template <typename T> BasicThreeVector<T> operator+(const BasicThreeVector<T> & a, const BasicThreeVector<T> & b);
template <typename T> BasicThreeVector<T> operator-(const BasicThreeVector<T> & a, const BasicThreeVector<T> & b);
template <typename T> BasicThreeVector<T> operator *(const T s, const BasicThreeVector<T> & v);
template <typename T> BasicThreeVector<T> operator *(const BasicThreeVector<T> & v, T s);
template <typename T> BasicThreeVector<T> operator /(const BasicThreeVector<T> & v, T s);

// Type definitions:  ThreeVector & ComplexThreeVector:
typedef BasicThreeVector<double>                ThreeVector;
typedef BasicThreeVector<std::complex<double>>  ComplexThreeVector;





// Rotation
ThreeVector operator *(const Rotation & rot, const ThreeVector & v);



// Rotation 
ComplexThreeVector operator *(const Rotation & rot, const ComplexThreeVector & v);


#include "Spacetime/ThreeVector.icc"

#endif
