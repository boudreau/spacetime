#ifndef _Rotation_h_
#define _Rotation_h_
#include "Spacetime/ThreeVector.h"
#include "Eigen/Dense"
#include <map>
#include <memory>
class Rotation {

  // Describes an active rotation.  Has a law of composition.  Can 
  // return the irreducible representation of a rotation matrix in
  // D dimensions (j=1/2, j=1, j=3/2, j=2...)
  
 public:

  // Constructor.  By default it will make an identity element.
  Rotation();
  
  // Constructor.  Takes the rotation vector as input;
  explicit Rotation(const ThreeVector & nVector);
  
  // Copy Constructor:
  Rotation(const Rotation & source);

  // Composition:
  Rotation operator * (const Rotation & source);

  // Get the dim-dimensional representation 
  const Eigen::MatrixXcd & rep(unsigned int dim) const;

  // Get the rotation vector.
  const ThreeVector & getRotationVector() const;

  // Gets the inverse rotation
  Rotation inverse() const;

  // Assignment operator:
  Rotation & operator = (const Rotation & source);

 private:

 

  // The direction of this vector is the axis of rotation.
  // Its length is the rotation angle.
  mutable std::unique_ptr<ThreeVector> nVector;
    
  // Here we hold the matrices of the transformation in various representations
  mutable std::map<unsigned int, Eigen::MatrixXcd> representation;


};
std::ostream & operator << (std::ostream & o, Rotation & t);
#endif

