#ifndef _WeylSpinor_h_
#define _WeylSpinor_h_
#include "Spacetime/FourVector.h"
#include "Spacetime/ThreeVector.h"
#include "Eigen/Dense"

class LorentzTransformation;

// Left and Right handed spinors inherit all of their properties from
// Eigen::Vector2cd; and in addition to that they can be acted upon by
// Lorentz Transformations.  


namespace WeylSpinor {

  enum Type {RightHandedType, LeftHandedType};
  
  template <int T>
  class BasicSpinor : public  Eigen::Vector2cd
  {
  public:

    // Default Constructor constructs null spinor
    BasicSpinor();
    
    // Constructor
    BasicSpinor(const ThreeVector & p);

    // This constructor allows you to construct a spinor from Eigen expressions
    template<typename Derived> BasicSpinor(const Eigen::MatrixBase<Derived>& other);
    
    // This method allows you to assign Eigen expressions to a spinor
    template<typename Derived> BasicSpinor & operator= (const Eigen::MatrixBase <Derived>& other);
  };
  
  // Create two distinct new datatypes, WeylSpinor::Left and WeylSpinor::Right:
  typedef BasicSpinor<LeftHandedType>  Left;
  typedef BasicSpinor<RightHandedType> Right;

}

// Action of Rotation upon the two types of Spinor:

WeylSpinor::Left  operator * (const LorentzTransformation & L, const WeylSpinor::Left   & s); 
WeylSpinor::Right operator * (const LorentzTransformation & L, const WeylSpinor::Right  & s);

// Return the Four-momentum of this Weyl Spinor
FourVector fourMomentum(const WeylSpinor::Left  &s);
FourVector fourMomentum(const WeylSpinor::Right &s);

// Return the Spin (magnitude and direction)
ThreeVector spin(const WeylSpinor::Left  &s);
ThreeVector spin(const WeylSpinor::Right &s);



// Some Gory Details are in the .icc file;  
#include "Spacetime/WeylSpinor.icc"

#endif
