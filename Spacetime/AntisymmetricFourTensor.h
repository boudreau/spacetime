#ifndef _AntisymmetricFourTensor_h_
#include "Spacetime/ddouble.h"
#include <iostream>
#include <complex>
class LorentzTransformation;

class AntisymmetricFourTensor {

 public:

  // Construct a zero four vector
  AntisymmetricFourTensor();

  // Copy constructor:
  AntisymmetricFourTensor (const AntisymmetricFourTensor & source);

  // Access to elements.  Class ddouble is used just like a double. 
  const ddouble & operator() (unsigned int i, unsigned int j) const;
  ddouble & operator() (unsigned int i, unsigned int jj);

  // Compound operations
  AntisymmetricFourTensor & operator +=(const AntisymmetricFourTensor & right);
  AntisymmetricFourTensor & operator -=(const AntisymmetricFourTensor & right);
  AntisymmetricFourTensor & operator *=(double s);

  // Unary minus:
  AntisymmetricFourTensor operator- () const;

  // Assignment operator:
  AntisymmetricFourTensor operator=(const AntisymmetricFourTensor & source);
  
 private:
  
  std::pair<double,double>  pair01, pair02, pair03, pair12, pair23, pair31, pair00;
  ddouble  x01, x10;
  ddouble  x02, x20;
  ddouble  x03, x30;
  ddouble  x12, x21;
  ddouble  x23, x32;
  ddouble  x31, x13;
  ddouble       m00; 
  const ddouble x00;

};


// Other operations:

std::ostream & operator << (std::ostream & o, const AntisymmetricFourTensor &v);
AntisymmetricFourTensor operator+(const AntisymmetricFourTensor & a, const AntisymmetricFourTensor & b);
AntisymmetricFourTensor operator-(const AntisymmetricFourTensor & a, const AntisymmetricFourTensor & b);
AntisymmetricFourTensor operator *(const double s, const AntisymmetricFourTensor & v);
AntisymmetricFourTensor operator *(const AntisymmetricFourTensor & v, double s);


// Lorentz Transformation:
AntisymmetricFourTensor operator *(const LorentzTransformation & transform, const AntisymmetricFourTensor & v);


#include "Spacetime/AntisymmetricFourTensor.icc"
#endif
