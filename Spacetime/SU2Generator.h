#ifndef _SU2Generator_h_
#define _SU2Generator_h_
#include "Eigen/Dense"
#include <map>
// This class consists of just a constructor:

class SU2Generator {

 public:

  // Access to the generators, held in cache:
  static const Eigen::MatrixXcd & JX(unsigned int dim);
  static const Eigen::MatrixXcd & JY(unsigned int dim);
  static const Eigen::MatrixXcd & JZ(unsigned int dim);

  // The following routine exponentiates linear combinations of SU2 generators
  // in an arbitrary dimensional space. It exponentiates them by first
  // diagonalizing them.  
  static Eigen::MatrixXcd exponentiate(int d, const Eigen::Vector3cd & nHat);

  // Takes the logarithm by diagonalizing the matrix:
  static Eigen::MatrixXcd logarithm(const Eigen::MatrixXcd &source);

 private:

  // No instances of this class:
  SU2Generator();
  
  // For speed, we cache the generators:
  static std::map<unsigned int, Eigen::MatrixXcd> JXR;
  static std::map<unsigned int, Eigen::MatrixXcd> JYR;
  static std::map<unsigned int, Eigen::MatrixXcd> JZR;

  

};
#endif
