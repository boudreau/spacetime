//=======================class ddouble========= ================//
//
// This class is used within AntisymmetricFourTensor and is
// useful potentially for any type of antisymmetric tensor
// It allows programmers to arrange for a pair of objects to
// be anti-synchronized.  When one member of the pair is
// assigned, the other member is assigned its opposite.  It
// can be used within a class like this:
// ...
//  private:
//
//    std::pair<double,double> xpair;
//    ddouble x12(xpair,true);
//    ddouble x21(xpair,false);
//  };
//
//  Then either ddouble can be assigned an accessed and used
//  like an ordinary double, but changes to x21 are propagated
//  automatically to x12=-x21 and vice versa. 
//
//==============================================================//
#ifndef _DDOUBLE_H_
#define _DDOUBLE_H_
#include <utility>
#include <complex>
class ddouble {

public:

  // Constructor:
  ddouble (std::pair<double, double> &pair, bool anti): pair(pair), anti(anti){};

  // cast to an ordinary double
  operator double() const  {
    return anti ? pair.second : pair.first;
  }

  // addition assignment
  ddouble & operator += (double x) {
    if (anti) {
      pair.first = -(pair.second+=x);
    }
    else{
      pair.second = -(pair.first+=x);
    }
    return *this;
  }

  // multiplication assignment
  ddouble & operator *= (double x) {
    if (anti) {
      pair.first = -(pair.second*=x);
    }
    else{
      pair.second = -(pair.first*=x);
    }
    return *this;
  }

  // subtraction assignment
  ddouble & operator -= (double x) {
    if (anti) {
      pair.first = -(pair.second-=x);
    }
    else{
      pair.second = -(pair.first-=x);
    }
    return *this;
  }
  
  // assignement
  ddouble & operator = (double x) {
    if (anti) {
      pair.first = -(pair.second=x);
    }
    else{
      pair.second = -(pair.first=x);
    }
    return *this;
  }
 private:
  std::pair<double,double> & pair;
  bool anti;
};

// The following assure interoperability with complex numbers:

// addition
std::complex<double> operator +(const ddouble & d, const std::complex<double> & c) {
  double x=d;
  return c+x;
}  

// addition
std::complex<double> operator +(const std::complex<double> & c, const ddouble & d) {
  double x=d;
  return c+x;
}  

// subtraction
std::complex<double> operator -(const ddouble & d, const std::complex<double> & c) {
  double x=d;
  return x-c;
}  

// subtraction
std::complex<double> operator -(const std::complex<double> & c, const ddouble & d) {
  double x=d;
  return c-x;
}  

// multiplication
std::complex<double> operator *(const ddouble & d, const std::complex<double> & c) {
  double x=d;
  return c*x;
}

// multiplication
std::complex<double> operator *(const std::complex<double> & c, const ddouble & d) {
  double x=d;
  return c*x;
}

// division
std::complex<double> operator /(const ddouble & d, const std::complex<double> & c) {
  double x=d;
  return x/c;
}

// division
std::complex<double> operator /(const std::complex<double> & c, const ddouble & d) {
  double x=d;
  return c/x;
}

#endif
