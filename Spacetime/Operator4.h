#ifndef _Operator4_h_
#define _Operator4_h_
#include "Spacetime/FourVector.h"
#include  "Eigen/Dense"
class AbsOperator4 {
 public:

  // Constructor:
  AbsOperator4();

  // Destructor();
  virtual ~AbsOperator4()=0;

  // Readonly access to elements:
  virtual const Eigen::Matrix4cd & operator[](unsigned int i) const = 0;

  // Clone:
  virtual const AbsOperator4 *clone() const=0;
  
};

class Operator4:public AbsOperator4 {
 public:

  // Constructor:
  Operator4();

  // Destructor();
  virtual ~Operator4();

  // Readonly access to elements:
  virtual const Eigen::Matrix4cd & operator[](unsigned int i) const;
  
  // Read/write access to the elements:
  Eigen::Matrix4cd & operator[](unsigned int i);

  // Clone:
  virtual const Operator4 *clone() const;
  
 protected:

  Eigen::Matrix4cd data[4];
  
};

class AbsOperator4x4 {
 public:

  // Constructor:
  AbsOperator4x4();

  // Destructor();
  virtual ~AbsOperator4x4()=0;

  // Readonly access to elements:
  virtual const Eigen::Matrix4cd & operator()(unsigned int i, unsigned int j) const = 0;

  // Clone:
  virtual const AbsOperator4x4 *clone() const=0;

  // Take a dot product with a four vector to return an Operator 4, eg  sigma.dot(q);
  Operator4 dot(const FourVector & p);
  
};


Operator4 operator * (const std::complex<double> & , const AbsOperator4 &);
Operator4 operator * (const AbsOperator4 &, const std::complex<double> & );

Operator4 operator * (const Eigen::Matrix4cd & , const AbsOperator4 &);
Operator4 operator * (const AbsOperator4 &, const Eigen::Matrix4cd  &);

Operator4 operator + (const AbsOperator4 & , const AbsOperator4 &);
Operator4 operator - (const AbsOperator4 & , const AbsOperator4 &);


#endif
