#ifndef _LorentzTransformation_h_
#define _LorentzTransformation_h_
#include "Spacetime/ThreeVector.h"
#include "Eigen/Dense"
#include <map>
#include <memory>
class LorentzTransformation {

  // Describes an active Lorentz Transformation.  Has a law of composition.  

  // Lorentz transformations act on rank-2 tensors, in general:
  // 
  // Weyl spinors:                  1/2 
  // Dirac spinors                  1/2 + 1/2 
  // Four vectors                   1/2 x 1/2.
  // AntiSymmetric 4-Tensors        1   +   1
  // 
  // It up to these objects to transcribe themselves into the proper representation
  // and apply the Lorentz Transformation, which furnishes two matrices acting on the
  // each piece of the rank 2 tensor.  
  
 public:

  // Constructor.  By default it will make an identity element.
  LorentzTransformation();
  
  // Constructor.  Takes the rapidity vector and the rotation vector:
  LorentzTransformation(const ThreeVector & rapVector,
			const ThreeVector & rotVector=ThreeVector(0,0,0));
  
  // Copy Constructor:
  LorentzTransformation(const LorentzTransformation & source);

  // Composition:
  LorentzTransformation operator * (const LorentzTransformation & source);

  // Get the dim-dimensional representation, matrix 1 and matrix 2
  const Eigen::MatrixXcd & rep1(unsigned int dim) const;
  const Eigen::MatrixXcd & rep2(unsigned int dim) const;

  // Get the rotation vector and the rapidity vector:
  ThreeVector getRotationVector() const;
  ThreeVector getRapidityVector() const;

  // Gets the inverse rotation
  LorentzTransformation inverse() const;

  // Assignment operator
  LorentzTransformation & operator=(const LorentzTransformation & source);

 private:

  // The direction of this vector is the axis of rotation.
  // Its length is the rotation angle.
  mutable std::unique_ptr<Eigen::Vector3cd> nVector;

  // Here we hold the matrices of the transformation in various representations
  mutable std::map<unsigned int, Eigen::MatrixXcd> representation1;
  mutable std::map<unsigned int, Eigen::MatrixXcd> representation2;

  // When Lorentz transformations are composed the vector is empty.  We can
  // create it here if from the simplest representation, which is used for
  // composition of Lorentz transformations.  Otherwise creates a null
  // vector ==> Identity trasnformation. 
  void createVector() const;
};
std::ostream & operator << (std::ostream & o, LorentzTransformation & t);
#endif

