//==========================================================================//
//                                                                          //
//  Special operators. This header file defines Pauli spin matrices         //
//                     sigma0, sigma1, sigma2, sigma3                       //
//                                                                          //
//                     and gamma matrices                                   //
//                     gamma0, gamma1, gamma2, gamma3, gamma5               //
// =========================================================================//
#ifndef SpecialOperators_h_
#define SpecialOperators_h_
#include "Spacetime/Operator4.h"

#include "Eigen/Dense"

namespace SpecialOperators {
  
  // Define I=sqrt(-1);
  const std::complex<double> I(0,1);
  
  // Commutator.  To facilitate initialization:
  inline Eigen::Matrix4cd cm(const Eigen::Matrix4cd & a, const Eigen::Matrix4cd & b) { return a*b-b*a;}
  
  // Define the Pauli matrices (I, s1, s2, s3, s4) 
  const Eigen::Matrix2cd sigmaNull = Eigen::Matrix2cd::Zero();
  
  const Eigen::Matrix2cd sigma0 = (
				   Eigen::Matrix2cd() <<
				   1, 0,
				   0, 1).finished();
  
  const Eigen::Matrix2cd sigma1 = (
				   Eigen::Matrix2cd() <<
				   0, 1,
				   1, 0).finished();
  
  const Eigen::Matrix2cd sigma2 = (
				   Eigen::Matrix2cd() <<
				   0, -I,
				   I, 0).finished();
  
  const Eigen::Matrix2cd sigma3 = (
				   Eigen::Matrix2cd() <<
				   1,  0,
				   0, -1).finished();
  
  // Define the gamma matrices:
  const Eigen::Matrix4cd gamma0 = (Eigen::Matrix4cd() <<
				   sigmaNull  , sigma0,
				   sigma0     , sigmaNull).         finished();
  
  const Eigen::Matrix4cd gamma1 = (Eigen::Matrix4cd() <<
				   sigmaNull  , sigma1,
				   -sigma1    , -sigmaNull).        finished();
  
  const Eigen::Matrix4cd gamma2 = (Eigen::Matrix4cd() <<
				   sigmaNull  , sigma2,
				   -sigma2    , sigmaNull).         finished();
  
  const Eigen::Matrix4cd gamma3 = (Eigen::Matrix4cd() <<
				   sigmaNull  , sigma3,
				   -sigma3    , sigmaNull).         finished();
  
  
  const Eigen::Matrix4cd gamma5 = (Eigen::Matrix4cd() <<
				   -sigma0    , sigmaNull,
				   sigmaNull , sigma0).            finished();
  
  
  const Eigen::Matrix4cd gamma[4] = {gamma0, gamma1, gamma2, gamma3};
  
  
  const Eigen::MatrixXcd PL=(Eigen::Matrix4cd::Identity()-gamma5)/2.0;
  const Eigen::MatrixXcd PR=(Eigen::Matrix4cd::Identity()+gamma5)/2.0;
  
  // Define the sigma matrices:
  const Eigen::Matrix4cd sigma[4][4] = {{I/2.0*cm(gamma0,gamma0), I/2.0*cm(gamma0,gamma1), I/2.0*cm(gamma0,gamma2), I/2.0*cm(gamma0,gamma3)},
					{I/2.0*cm(gamma1,gamma0), I/2.0*cm(gamma1,gamma1), I/2.0*cm(gamma1,gamma2), I/2.0*cm(gamma1,gamma3)},
					{I/2.0*cm(gamma2,gamma0), I/2.0*cm(gamma2,gamma1), I/2.0*cm(gamma2,gamma2), I/2.0*cm(gamma2,gamma3)},
					{I/2.0*cm(gamma3,gamma0), I/2.0*cm(gamma3,gamma1), I/2.0*cm(gamma3,gamma2), I/2.0*cm(gamma3,gamma3)}};

  const Eigen::Matrix4cd S[4][4]     = {{sigma[0][0]/2, sigma[0][1]/2, sigma[0][2]/2, sigma[0][3]/2},
					{sigma[1][0]/2, sigma[1][1]/2, sigma[1][2]/2, sigma[1][3]/2},
					{sigma[2][0]/2, sigma[2][1]/2, sigma[2][2]/2, sigma[2][3]/2},
					{sigma[3][0]/2, sigma[3][1]/2, sigma[3][2]/2, sigma[3][3]/2}};
  
  
  
  class Gamma: public AbsOperator4 {
  public:
    
    // Constructor:
    Gamma(){};
    
    // Destructor:
    ~Gamma(){}

    // Clone:
    virtual const Gamma *clone() const { return new Gamma(*this);}

    // Return one of the gamma matrices;
    const Eigen::Matrix4cd & operator[] (unsigned int i) const {
      return gamma[i];
    }
  };
  
  class Sigma4x4: public AbsOperator4x4 {
  public:
    
    // Constructor:
    Sigma4x4(){};
    
    // Destructor:
    ~Sigma4x4(){}

    // Clone:
    virtual const Sigma4x4 *clone() const { return new Sigma4x4(*this);}

    // Return one of the gamma matrices;
    const Eigen::Matrix4cd & operator() (unsigned int i, unsigned int j) const {
      return sigma[i][j];
    }

    // 
    Operator4 dot(const FourVector & p) const {
      Operator4 result;
      for (int mu=0;mu<4;mu++) {
	result[mu] += I*sigma[mu][0]*p[0];
	for (int nu=1;nu<4;nu++) {
	  result[mu] -= I*sigma[mu][nu]*p[nu];
	}
      }
      return result;
    }
    
  };
  
  

  inline Eigen::MatrixXcd slash(const FourVector & p) {
    return p[0]*gamma0 - p[1]*gamma1 -p[2]*gamma[2]-p[3]*gamma3;
  }

  inline Eigen::MatrixXcd slash(const ComplexFourVector & p) {
    return p[0]*gamma0 - p[1]*gamma1 -p[2]*gamma[2]-p[3]*gamma3;
  }

  // Internal use only:------------------------
#include "SpecialOperators.icc"
  //--------------------------------------------
}

#endif
