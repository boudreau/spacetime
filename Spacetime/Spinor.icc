#include "Spacetime/Spinor.h"
#include "Spacetime/Rotation.h"
#include "Spacetime/ThreeVector.h"
#include "Spacetime/SU2Generator.h"

template<unsigned int D>
Spinor<D>::Spinor():
  Eigen::Matrix<std::complex<double>,D,1>(Eigen::Matrix<std::complex<double>,D,1>::Zero())
  {}

template<unsigned int D>
Spinor<D>::Spinor(const std::initializer_list<std::complex<double>> & list):
  Eigen::Matrix<std::complex<double>,D,1>(Eigen::Matrix<std::complex<double>,D,1>::Zero())
{
  unsigned int c(0);
  for (auto i=list.begin(); i<list.end();i++) {
    (*this)(c++)=*i;
  }
}

template<unsigned int D>
template<typename Derived>
Spinor<D>::Spinor(const Eigen::MatrixBase<Derived>& other) : Eigen::Matrix<std::complex<double>,D,1> (other)
{ }


template<unsigned int D>
template<typename Derived>
Spinor<D> & Spinor<D>::operator=(const Eigen::MatrixBase<Derived>& other)
{
  this->Eigen::Matrix<std::complex<double>,D,1>::operator=(other);
  return *this;
}

template<unsigned int D>
inline Spinor<D> operator * (const Rotation & R, const Spinor<D> & X) {
  return  R.rep(D)*X;
}

template<unsigned int D>
inline ThreeVector spin(const Spinor<D> & s) {
  return ThreeVector((s.adjoint()*SU2Generator::JX(D)*s)(0,0).real(),
		     (s.adjoint()*SU2Generator::JY(D)*s)(0,0).real(),
		     (s.adjoint()*SU2Generator::JZ(D)*s)(0,0).real());
}
