#ifndef _Spinor_h_
#define _Spinor_h_
#include "Spacetime/ThreeVector.h"
#include "Eigen/Dense"
#include <initializer_list>
class Rotation;
template <unsigned int D=2>
  class Spinor : public  Eigen::Matrix<std::complex<double>,D,1> {
 public:
  
  // Default Constructor constructs null spinor
  Spinor();
  
  // Constructor
  Spinor(const std::initializer_list<std::complex<double>> & values);
  
  // This constructor allows you to construct a spinor from Eigen expressions
  template<typename Derived> Spinor(const Eigen::MatrixBase<Derived>& other);
  
  // This method allows you to assign Eigen expressions to a spinor
  template<typename Derived> Spinor<D> & operator= (const Eigen::MatrixBase <Derived>& other);
};

// Action of Rotation upon Spinor:
template <unsigned int D>
Spinor<D> operator * (const Rotation & R, const Spinor<D> & v); 

// Recover the spin from the Spinor
template <unsigned int D>
ThreeVector spin(const Spinor<D> & s);

#include "Spacetime/Spinor.icc"
#endif
