#ifndef _DiracSpinor_h_
#define _DiracSpinor_h_
// ================================================================//
//                                                                 //
//   This header file defines the classes:                         //
//                                                                 //
//   DiracSpinor::U                                                //
//   DiracSpinor::V                                                //
//   DiracSpinor::Any                                              //
//   DiracSpinor::Bar                                              //
//                                                                 //
// ================================================================//

#include "Spacetime/FourVector.h"
#include "Spacetime/ThreeVector.h"
#include "Spacetime/WeylSpinor.h"
#include "Spacetime/Rotation.h"
#include "Spacetime/Spinor.h"
#include "Eigen/Dense"

class LorentzTransformation;

namespace DiracSpinor {
  
  enum Type {UType,VType};
  
  template <int T> 
    class BasicSpinor : public  Eigen::Vector4cd
    {
    public:
      
      // Default Constructor constructs null spinor
      BasicSpinor();
      
      // Constructor
      BasicSpinor(const Spinor<2> & s, double mass);
      
      // Construct from a spinor and a four-vector:
      BasicSpinor(const Spinor<2> & s, const FourVector & p);
      
      // Construct from a Weyl spinor to obtain a Dirac spinor representing a
      // massless particle or antiparticle:
      BasicSpinor(const WeylSpinor::Left  & s);
      BasicSpinor(const WeylSpinor::Right & s);
      
      // This constructor allows you to construct a spinor from Eigen expressions
      template<typename Derived> BasicSpinor(const Eigen::MatrixBase<Derived>& other);
      
      // This method allows you to assign Eigen expressions to a spinor
      template<typename Derived> BasicSpinor & operator= (const Eigen::MatrixBase <Derived>& other);
    };



  class Bar : public  Eigen::RowVector4cd
  {
  public:
    
    // Default Constructor constructs null spinor
    Bar();
    
    // Constructor
    Bar(const std::complex<double> & s0,
	const std::complex<double> & s1,
	const std::complex<double> & s2,
	const std::complex<double> & s3
	);
    
    // This constructor allows you to construct a spinor from Eigen expressions
    template<typename Derived> Bar(const Eigen::MatrixBase<Derived>& other);
    
    // This method allows you to assign Eigen expressions to a spinor
    template<typename Derived> Bar & operator= (const Eigen::MatrixBase <Derived>& other);
  };
  
  class Any : public  Eigen::Vector4cd
  {
  public:
    
    // This constructor allows you to construct a spinor from Eigen expressions
    template<typename Derived> Any(const Eigen::MatrixBase<Derived>& other);
    
    // This method allows you to assign Eigen expressions to a spinor
    template<typename Derived> Any & operator= (const Eigen::MatrixBase <Derived>& other);
  };
  
  // Create two distinct new datatypes, DiracSpinor::U and DiracSpinor::V
  typedef BasicSpinor<UType> U;
  typedef BasicSpinor<VType> V;
}


// Take the spinor-bar:
DiracSpinor::Bar bar(const DiracSpinor::U & u);
DiracSpinor::Bar bar(const DiracSpinor::V & v);

// Action of Lorentz Transformation upon the spinor
DiracSpinor::U     operator * (const LorentzTransformation & L, const DiracSpinor::U      & u); 
DiracSpinor::V     operator * (const LorentzTransformation & L, const DiracSpinor::V      & v); 

// Return the Four-momentum of this Dirac Spinor
FourVector fourMomentum(const DiracSpinor::U &u);
FourVector fourMomentum(const DiracSpinor::V &v);

// Return the Spin (magnitude and direction)
ThreeVector spin(const DiracSpinor::U  &u);
ThreeVector spin(const DiracSpinor::V  &v);


// Some Gory Details are in the .icc file;  
#include "Spacetime/DiracSpinor.icc"

#endif
