#ifndef _FourVector_h_
#define _FourVector_h_
#include "Spacetime/ThreeVector.h"
#include <complex>
class LorentzTransformation;
template <typename T>
class BasicFourVector {

 public:

  // Construct a zero four vector
  BasicFourVector();

  // Construct from four components
  BasicFourVector(T x0, T x1, T x2, T x3);

  // Access to elements:
  const T & operator[] (unsigned int i) const;
  T & operator[] (unsigned int i);

  // Access to elements: 
  const T & operator() (unsigned int i) const;
  T & operator() (unsigned int i);

  // Return the space portion:
  BasicThreeVector<T> space() const;

  // Returns the invariant interval:
  T dot(const BasicFourVector<T> & source) const;

  // Compound operations
  BasicFourVector<T> & operator +=(const BasicFourVector<T> & right);
  BasicFourVector<T> & operator -=(const BasicFourVector<T> & right);
  BasicFourVector<T> & operator *=(T s);
  BasicFourVector<T> & operator /=(T s);

  // Unary minus:
  BasicFourVector<T> operator- () const;

  // Conjugate
  BasicFourVector<T> conjugate() const;
  
  // Norm:
  double norm() const;

  // Squared norm:
  double squaredNorm() const;

  
  
 private:

  // Store the components of this four-vector;
  T x[4];
  
};


// Other operations:
template <typename T> std::ostream & operator << (std::ostream & o, const BasicFourVector<T> &v);
template <typename T> BasicFourVector<T> operator+(const BasicFourVector<T> & a, const BasicFourVector<T> & b);
template <typename T> BasicFourVector<T> operator-(const BasicFourVector<T> & a, const BasicFourVector<T> & b);
template <typename T> BasicFourVector<T> operator *(const T s, const BasicFourVector<T> & v);
template <typename T> BasicFourVector<T> operator *(const BasicFourVector<T> & v, T s);
template <typename T> BasicFourVector<T> operator /(const BasicFourVector<T> & v, T s);

// Type definitions:  FourVector & ComplexFourVector:
typedef BasicFourVector<double>                FourVector;
typedef BasicFourVector<std::complex<double>>  ComplexFourVector;





// Lorentz Transformation:
FourVector operator *(const LorentzTransformation & transform, const FourVector & v);



// Lorentz Transformation:
ComplexFourVector operator *(const LorentzTransformation & transform, const ComplexFourVector & v);

#include "Spacetime/FourVector.icc"

#endif
