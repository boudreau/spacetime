# Spacetime

The Spacetime library manipulates vectors, four-vectors, spinors, Dirac and Weyl spinors, Lorentz transformations, four-currents, gamma matrices and other such objects that appear in Feynman diagrams. 

## Documentation

-  qat.pitt.edu


## Dependencies

- Eigen: http://eigen.tuxfamily.org/


## Build instructions

```
git clone https://gitlab.cern.ch/boudreau/spacetime.git
cd spacetime
mkdir build
cd build
cmake ..
make
sudo make install
```
