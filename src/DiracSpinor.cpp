#include "Spacetime/DiracSpinor.h"
#include "Spacetime/LorentzTransformation.h"
#include "Spacetime/SpecialOperators.h"
DiracSpinor::U    operator * ( const LorentzTransformation & T, const DiracSpinor::U & s) {
  //
  // This computation is carried out in the Weyl representation. Top part 
  // according to the left handed representation, bottom according to the 
  // right handed representation:
  DiracSpinor::U result;
  Eigen::Vector2cd sL=T.rep2(2)*s.head(2); 
  Eigen::Vector2cd sR=T.rep1(2)*s.tail(2); 
  result.head(2)=sL;
  result.tail(2)=sR;
  return result;

}
DiracSpinor::V    operator * ( const LorentzTransformation & T, const DiracSpinor::V & s) {
  //
  // This computation is carried out in the Weyl representation. Top part 
  // according to the left handed representation, bottom according to the 
  // right handed representation:
  DiracSpinor::V result;
  Eigen::Vector2cd sL=T.rep2(2)*s.head(2); 
  Eigen::Vector2cd sR=T.rep1(2)*s.tail(2); 
  result.head(2)=sL;
  result.tail(2)=sR;
  return result;
}


DiracSpinor::Bar bar(const DiracSpinor::U & s) {
  DiracSpinor::Bar result;
  result.topLeftCorner(1,2)=s.tail(2).adjoint();
  result.bottomRightCorner(1,2)=s.head(2).adjoint();
  return result;
}

DiracSpinor::Bar bar(const DiracSpinor::V & s) {
  DiracSpinor::Bar result;
  result.topLeftCorner(1,2)=s.tail(2).adjoint();
  result.bottomRightCorner(1,2)=s.head(2).adjoint();
  return result;
}



FourVector fourMomentum(const DiracSpinor::U &s) {
  using namespace SpecialOperators;
  return FourVector(
		    (bar(s)*gamma0*s)(0,0).real()/2.0,
		    (bar(s)*gamma1*s)(0,0).real()/2.0,
		    (bar(s)*gamma2*s)(0,0).real()/2.0,
		    (bar(s)*gamma3*s)(0,0).real()/2.0);
}

FourVector fourMomentum(const DiracSpinor::V &s) {
  using namespace SpecialOperators;
  return FourVector(
		    (bar(s)*gamma0*s)(0,0).real()/2.0,
		    (bar(s)*gamma1*s)(0,0).real()/2.0,
		    (bar(s)*gamma2*s)(0,0).real()/2.0,
		    (bar(s)*gamma3*s)(0,0).real()/2.0);
}


ThreeVector spin(const DiracSpinor::U &s) {
  using namespace SpecialOperators;
  double  m=(s.adjoint()*s)(0,0).real();
  return ThreeVector(
		     (s.adjoint()*S[2][3]*s)(0,0).real()/m,
		     (s.adjoint()*S[3][1]*s)(0,0).real()/m,
		     (s.adjoint()*S[1][2]*s)(0,0).real()/m);
}


ThreeVector spin(const DiracSpinor::V &s) {
  using namespace SpecialOperators;
  double  m=(s.adjoint()*s)(0,0).real();
  return ThreeVector(
		     (-s.adjoint()*S[2][3]*s)(0,0).real()/m,
		     (-s.adjoint()*S[3][1]*s)(0,0).real()/m,
		     (-s.adjoint()*S[1][2]*s)(0,0).real()/m);
}


 
