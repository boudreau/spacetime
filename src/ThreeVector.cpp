#include "Spacetime/ThreeVector.h"
#include "Spacetime/Rotation.h"
#include "Eigen/Dense"
ThreeVector operator * (const Rotation & R, const ThreeVector & v) {
  typedef std::complex<double> Complex; 
  static const double sqrt2=sqrt(2.0);
  // Fill a complex vector:
  Eigen::Vector3cd
    X(Complex(-v(0),  +v(1))/sqrt2,
      Complex(+v(2)),
      Complex(+v(0), +v(1))/sqrt2);

  // Rotate the complex vector:
  Eigen::Vector3cd XPrime=R.rep(3)*X;

  // Re-extract x,y,z values from the rotated complex vector.
  return ThreeVector((XPrime(2)-XPrime(0)).real()/sqrt2,
		     (XPrime(0)+XPrime(2)).imag()/sqrt2,
		     XPrime(1).real());
		       
}

ComplexThreeVector operator * (const Rotation & R, const ComplexThreeVector & v) {
  typedef std::complex<double> Complex; 
  Complex I(0,1);
  static const double sqrt2=sqrt(2.0);
  // Fill a complex vector:
  Eigen::Vector3cd
    X((-v(0)+I*v(1))/sqrt2,
      v(2),
      (+v(0) +I*v(1))/sqrt2);
  
  // Rotate the complex vector:
  Eigen::Vector3cd XPrime=R.rep(3)*X;
  
  // Re-extract x,y,z values from the rotated complex vector.
  return ComplexThreeVector(
			    (XPrime(2)-XPrime(0))/sqrt2,
			    -I*(XPrime(0)+XPrime(2))/sqrt2,
			    XPrime(1));
  
}
