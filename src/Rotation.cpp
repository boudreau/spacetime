#include "Spacetime/Rotation.h"
#include "Spacetime/SU2Generator.h"
#include <iostream>

using namespace std;
using namespace Eigen;
typedef complex<double> Complex;

Rotation::Rotation() {

}

Rotation::Rotation(const Rotation & source):
  nVector(source.nVector ? unique_ptr<ThreeVector>(new ThreeVector(*source.nVector)) : unique_ptr<ThreeVector>()),
  representation(source.representation)
{}

Rotation & Rotation::operator= (const Rotation & source) {
  if (this != & source) {
    nVector = source.nVector ? unique_ptr<ThreeVector>(new ThreeVector(*source.nVector)) : unique_ptr<ThreeVector>();
    representation = source.representation;
  }
  return *this;
}
Rotation::Rotation(const ThreeVector & nVector) :
  nVector(new ThreeVector(nVector))
{
}

Rotation Rotation::inverse() const {
  return Rotation(-getRotationVector());
}

std::ostream & operator << (std::ostream & o, Rotation & t) {
  std::cout << "Rotation" << std::endl;
  std::cout << t.getRotationVector() << std::endl;
  return o;
}

const ThreeVector & Rotation::getRotationVector() const {
  if (!nVector) {
    auto it=representation.find(2);
    if (it==representation.end()) {
      nVector.reset(new ThreeVector(0,0,0));
    }
    else {
      //
      // Get the rotation axis and angle from this.
      //
      Eigen::MatrixXcd X=(*it).second;
      ComplexEigenSolver<Eigen::MatrixXcd> solver(X);
    
      MatrixXcd ev=solver.eigenvectors().col(1), evAdjoint=ev.adjoint();
      ThreeVector v(
		 (evAdjoint*SU2Generator::JX(2)*ev)(0,0).real(),
		 (evAdjoint*SU2Generator::JY(2)*ev)(0,0).real(),
		 (evAdjoint*SU2Generator::JZ(2)*ev)(0,0).real()
		 );
      nVector.reset(new ThreeVector(v*(-4.0*std::log(solver.eigenvalues()(1))).imag()));
			      
    }
  }
  return *nVector;
}


Rotation Rotation::operator * (const Rotation & source) {
  static const Complex I(0,1);
  Rotation transform;
  Eigen::MatrixXcd X=rep(2)*source.rep(2);
  transform.representation.insert(make_pair(2,X));
  return transform;
}

const Eigen::MatrixXcd & Rotation::rep(unsigned int dim) const {
  auto iterator = representation.find(dim);
  if (iterator == representation.end()) {
    static Complex I(0,1);
    Eigen::Vector3cd cnVector(getRotationVector()(0),
			      getRotationVector()(1),
			      getRotationVector()(2));
    Eigen::Vector3cd nHat=   -I*cnVector; 
    Eigen::MatrixXcd expArgMatrix=SU2Generator::exponentiate(dim,nHat);
    auto iPair = representation.insert(make_pair(dim,expArgMatrix));
    return (*iPair.first).second;
  }
  return (*iterator).second;
}


