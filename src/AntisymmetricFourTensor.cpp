#include "Spacetime/AntisymmetricFourTensor.h"
#include "Spacetime/LorentzTransformation.h"

#include "Eigen/Dense"

// Lorentz Transformation:
AntisymmetricFourTensor operator *(const LorentzTransformation & L, const AntisymmetricFourTensor & v) {
  typedef std::complex<double> Complex;
  Complex I(0,1);
  const double sqrt2=sqrt(2.0), sqrt8=sqrt(8);
  
  Eigen::Vector3cd
    S1((-v(1,0)-I*v(3,2) + I*v(2,0)-v(1,3))/sqrt2,
       v(3,0)+I*v(2,1),
       (v(1,0)+I*v(3,2) + I*v(2,0)-v(1,3))/sqrt2);

  Eigen::Vector3cd
    S2((-v(1,0)+I*v(3,2) + I*v(2,0)+v(1,3))/sqrt2,
       v(3,0)-I*v(2,1),
       (v(1,0)-I*v(3,2) + I*v(2,0)+v(1,3))/sqrt2);
    
  Eigen::Vector3cd S1Prime=L.rep1(3)*S1, S2Prime=L.rep2(3)*S2;
  
  AntisymmetricFourTensor T;
  T(1,0) = real(S1Prime(2)-S1Prime(0) + S2Prime(2)-S2Prime(0))/sqrt8;
  T(2,0) = real (-I*(S1Prime(2)+S1Prime(0)+S2Prime(2)+S2Prime(0)))/sqrt8;
  T(3,0) = real(S1Prime(1)+S2Prime(1))/2.0;
  
  T(3,2) = imag(S1Prime(2)-S1Prime(0) - S2Prime(2) +S2Prime(0))/sqrt8;
  T(1,3) = imag (-I*(S1Prime(2)+S1Prime(0)-S2Prime(2)-S2Prime(0)))/sqrt8;
  T(2,1) = imag(S1Prime(1)-S2Prime(1))/2.0;
  return T;
}
