#include "Spacetime/LorentzTransformation.h"
#include "Spacetime/SU2Generator.h"
#include <iostream>
#include <complex>
using namespace std;
using namespace Eigen;
typedef complex<double> Complex;

LorentzTransformation::LorentzTransformation() {
}

LorentzTransformation::LorentzTransformation(const ThreeVector & rapVector,
					     const ThreeVector & rotVector) 
  :
  nVector(new Vector3cd(rotVector(0)+Complex(0,1)*rapVector(0),
			rotVector(1)+Complex(0,1)*rapVector(1),
			rotVector(2)+Complex(0,1)*rapVector(2)))
{}


LorentzTransformation::LorentzTransformation(const LorentzTransformation & source):
  nVector(source.nVector ? unique_ptr<Vector3cd>(new Vector3cd(*source.nVector)) : unique_ptr<Vector3cd>()),
  representation1(source.representation1),
  representation2(source.representation2)
{}

LorentzTransformation & LorentzTransformation::operator= (const LorentzTransformation & source) {
  if (this != & source) {
    nVector = source.nVector ? unique_ptr<Vector3cd>(new Vector3cd(*source.nVector)) : unique_ptr<Vector3cd>();
    representation1 = source.representation1;
    representation2 = source.representation2;
  }
  return *this;
}

LorentzTransformation LorentzTransformation::inverse() const {
  createVector();
  Eigen::Vector3d rnVector=nVector->real();
  Eigen::Vector3d inVector=nVector->imag();
  LorentzTransformation t(ThreeVector(-inVector[0],-inVector[1],-inVector[2]),
			  ThreeVector(-rnVector[0],-rnVector[1],-rnVector[2]));

  return t;
}

std::ostream & operator << (std::ostream & o, LorentzTransformation & t) {
  std::cout << "LorentzTransformation" << std::endl;
  std::cout << "Rapidity: " << t.getRapidityVector() << std::endl;
  std::cout << "Rotation"   << t.getRotationVector() << std::endl;
  return o;
}

void LorentzTransformation::createVector() const {
  if (!nVector) {
    auto it=representation1.find(2);
    if (it==representation1.end()) {
      nVector.reset(new Vector3cd(0,0,0));
    }
    else {
      Eigen::MatrixXcd X=(*it).second;
      const Complex I(0,1);
      Eigen::MatrixXcd ilogX=I*SU2Generator::logarithm(X);
      Eigen::Vector3cd v(ilogX(0,1)+ilogX(1,0),
			 I*(ilogX(0,1)-ilogX(1,0)),
			 ilogX(0,0)-ilogX(1,1));
      nVector.reset( new Eigen::Vector3cd(v));
    }
  }
}

ThreeVector LorentzTransformation::getRotationVector() const {
  createVector();
  Eigen::Vector3d rV=nVector->real();
  return ThreeVector(rV[0],rV[1],rV[2]);
}

ThreeVector LorentzTransformation::getRapidityVector() const {
  createVector();
  Eigen::Vector3d iV=nVector->imag();
  return ThreeVector(iV[0],iV[1],iV[2]);
}

LorentzTransformation LorentzTransformation::operator * (const LorentzTransformation & source) {
  const Complex I(0,1);
  LorentzTransformation transform;
  Eigen::MatrixXcd X=rep1(2)*source.rep1(2);
  transform.representation1.insert(make_pair(2,X));
  return transform;
}

const Eigen::MatrixXcd & LorentzTransformation::rep1(unsigned int dim) const {
  auto iterator = representation1.find(dim);
  if (iterator == representation1.end()) {
    // exponentiates -I*nVector
    createVector();
    const Complex I(0,1);
    Eigen::MatrixXcd expArgMatrix=SU2Generator::exponentiate(dim,-I**nVector);
    auto iPair = representation1.insert(make_pair(dim,expArgMatrix));
    return (*iPair.first).second;
  }
  return (*iterator).second;
}

const Eigen::MatrixXcd & LorentzTransformation::rep2(unsigned int dim) const {
  auto iterator = representation2.find(dim);
  if (iterator == representation2.end()) {
    // exponentiates -I*nVector.conj()
    createVector();
    const Complex I(0,1);
    Eigen::MatrixXcd expArgMatrix=SU2Generator::exponentiate(dim,-I*nVector->conjugate());
    auto iPair = representation2.insert(make_pair(dim,expArgMatrix));
    return (*iPair.first).second;
  }
  return (*iterator).second;
}


