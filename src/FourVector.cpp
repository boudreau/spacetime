#include "Spacetime/FourVector.h"
#include "Spacetime/LorentzTransformation.h"
#include <iostream>

FourVector operator *(const LorentzTransformation & transform, const FourVector & v) {


  std::complex<double> I(0.0,1.0);
  const Eigen::MatrixXcd & AR=transform.rep1(2);
  const Eigen::MatrixXcd & AL=transform.rep2(2);
  Eigen::Matrix2cd V;
  V <<
    -v[1]+I*v[2],    v[3]+  v[0],
     v[3]-  v[0],    v[1]+I*v[2];

  Eigen::Matrix2cd VP=AR*V*AL.transpose();

  
  return FourVector(real(VP(0,1)-VP(1,0))/2.0,
		    real(VP(1,1)-VP(0,0))/2.0,
		    imag(VP(0,0)+VP(1,1))/2.0,
		    real(VP(0,1)+VP(1,0))/2.0);


}

ComplexFourVector operator *(const LorentzTransformation & transform, const ComplexFourVector & v) {


  std::complex<double> I(0.0,1.0);
  const Eigen::MatrixXcd & AR=transform.rep1(2);
  const Eigen::MatrixXcd & AL=transform.rep2(2);
  Eigen::Matrix2cd V;
  V <<
    -v[1]+I*v[2],    v[3]+  v[0],
     v[3]-  v[0],    v[1]+I*v[2];

  Eigen::Matrix2cd VP=AR*V*AL.transpose();

  
  return ComplexFourVector((VP(0,1)-VP(1,0))/2.0,
			   (VP(1,1)-VP(0,0))/2.0,
			   -I*(VP(0,0)+VP(1,1))/2.0,
			   (VP(0,1)+VP(1,0))/2.0);


}
