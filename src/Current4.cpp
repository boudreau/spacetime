#include "Spacetime/Current4.h"
#include <stdexcept>
Current4::operator ComplexFourVector () const {
  if (current) return *current;
  
  if (!spinor || !spinorBar || !op) {
    throw std::runtime_error("Current4:  Improperly formed current");
  }
  current = std::shared_ptr<ComplexFourVector>(new ComplexFourVector());
  for (int mu=0;mu<4;mu++) (*current)[mu] = ((*spinorBar)*((*op)[mu])*(*spinor))(0);
  return *current;
}

Current4 operator * (const DiracSpinor::Bar &spinorBar, const AbsOperator4 & op) {
  Current4 current;
  current.op=std::shared_ptr<const AbsOperator4> (op.clone());
  current.spinorBar=std::shared_ptr<DiracSpinor::Bar> (new DiracSpinor::Bar(spinorBar));
  return current;
}

Current4 operator * (const Current4 & current, const DiracSpinor::Any & spinor) {
  Current4 copyOfCurrent=current;
  copyOfCurrent.spinor=std::shared_ptr<DiracSpinor::Any> (new DiracSpinor::Any(spinor));
  return copyOfCurrent;
}
