#include "Spacetime/SU2Generator.h"
#include <stdexcept>
#include <cmath>
#include <mutex>
using namespace std;
using namespace Eigen;

map<unsigned int, Eigen::MatrixXcd> SU2Generator::JXR;
map<unsigned int, Eigen::MatrixXcd> SU2Generator::JYR;
map<unsigned int, Eigen::MatrixXcd> SU2Generator::JZR;
mutex su2generatorMutex;
typedef complex<double> Complex;

const MatrixXcd & SU2Generator::JX(unsigned int dim) {
  auto g=JXR.find(dim);
  if (g==JXR.end()) {
    double j=(dim-1)/2.0;
    MatrixXcd X=MatrixXcd::Zero(dim,dim);
    for (unsigned int i=0;i<dim;i++) {
      if (i<dim-1) {
	unsigned int k=i+1;
	X(i,k)=0.5*std::sqrt(k*(2.0*j-k+1));
      }
      if (i>0) {
	unsigned int k=i-1;
	X(i,k)=0.5*std::sqrt((2.0*j-k)*(k+1));
      }
    }
    su2generatorMutex.lock();
    auto g=JXR.insert(make_pair(dim,X));
    su2generatorMutex.unlock();
    return g.first->second;
  }
  return g->second;
}

const MatrixXcd & SU2Generator::JY(unsigned int dim) {
  auto g=JYR.find(dim);
  if (g==JYR.end()) {
    double j=(dim-1)/2.0;
    MatrixXcd Y=MatrixXcd::Zero(dim,dim);
    for (unsigned int i=0;i<dim;i++) {
      if (i<dim-1) {
	unsigned int k=i+1;
	Y(i,k)=Complex(0.0,-0.5)*std::sqrt(k*(2.0*j-k+1));
      }
      if (i>0) {
	unsigned int k=i-1;
	Y(i,k)=Complex(0.0,0.5)*std::sqrt((2.0*j-k)*(k+1));
      }
    }
    su2generatorMutex.lock();
    auto g=JYR.insert(make_pair(dim,Y));
    su2generatorMutex.unlock();
    return g.first->second;
  }
  return g->second;
}

const MatrixXcd & SU2Generator::JZ(unsigned int dim) {
  auto g=JZR.find(dim);
  if (g==JZR.end()) {
    double j=(dim-1)/2.0;
    MatrixXcd Z=MatrixXcd::Zero(dim,dim);
    for (unsigned int i=0;i<dim;i++) {
      Z(i,i)=double(j)-double(i);
    }
    su2generatorMutex.lock();
    auto g=JZR.insert(make_pair(dim,Z));
    su2generatorMutex.unlock();
    return g.first->second;
  }
  return g->second;
}


MatrixXcd SU2Generator::exponentiate(int d, const Vector3cd & nHat) {
  
  const Complex alpha = std::sqrt(nHat(0)*nHat(0)+nHat(1)*nHat(1)+nHat(2)*nHat(2));
  if (alpha==0.0) return MatrixXcd::Identity(d,d);
  
  //
  // Here is the matrix to be diagonalized:
  //
  MatrixXcd M=(nHat(0)*SU2Generator::JX(d)+nHat(1)*SU2Generator::JY(d)+nHat(2)*SU2Generator::JZ(d));
  MatrixXcd expD=MatrixXcd::Zero(d,d);

  ComplexEigenSolver<MatrixXcd> solver(M);
  MatrixXcd A=solver.eigenvectors();
  VectorXcd D=solver.eigenvalues();
  for (int e=0;e<d;e++) {
    expD(e,e)=std::exp(D(e));
  }
  return A*expD*A.inverse();
 
}

MatrixXcd SU2Generator::logarithm(const MatrixXcd &source) {

  int r=source.rows();
  int c=source.cols();
  if (r!=c) throw std::runtime_error("Error in SU2Generator::logarithm: non square matrix");
  int d=r;
  
  ComplexEigenSolver<MatrixXcd> solver(source);
  MatrixXcd A=solver.eigenvectors();
  VectorXcd D=solver.eigenvalues();
  MatrixXcd logD=MatrixXcd::Zero(source.rows(),source.cols());
  for (int e=0;e<d;e++) {
    logD(e,e)=std::log(D(e));
  }
  return A*logD*A.inverse();
}
