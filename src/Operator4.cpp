#include "Spacetime/Operator4.h"
AbsOperator4::AbsOperator4(){
}

AbsOperator4::~AbsOperator4(){
}

AbsOperator4x4::AbsOperator4x4(){
}

AbsOperator4x4::~AbsOperator4x4(){
}

// Constructor:
Operator4::Operator4() {
}

Operator4::~Operator4() {
}

// Readonly access to elements:
const Eigen::Matrix4cd & Operator4::operator[](unsigned int i) const {
  return data[i];
}
  
// Read/write access to the elements:
Eigen::Matrix4cd & Operator4::operator[](unsigned int i) {
  return data[i];
}

// Clone:
const Operator4 *Operator4::clone() const{
  return new Operator4(*this);
}


Operator4 operator * (const std::complex<double> & s, const AbsOperator4 &o) {
  Operator4 op;
  for (int i=0;i<4;i++) op[i]=s*o[i];
  return op;
}

Operator4 operator * (const AbsOperator4 & o, const std::complex<double> & s) {
  Operator4 op;
  for (int i=0;i<4;i++) op[i]=o[i]*s;
  return op;
}

Operator4 operator * (const Eigen::Matrix4cd & s , const AbsOperator4 & o) {
  Operator4 op;
  for (int i=0;i<4;i++) op[i]=s*o[i];
  return op;
}
Operator4 operator * (const AbsOperator4 & o, const Eigen::Matrix4cd  & s) {
  Operator4 op;
  for (int i=0;i<4;i++) op[i]=o[i]*s;
  return op;
}

Operator4 operator + (const AbsOperator4 & o1, const AbsOperator4 & o2) {
  Operator4 op;
  for (int i=0;i<4;i++) op[i]=o1[i]+o2[i];
  return op;
}

Operator4 operator - (const AbsOperator4 & o1, const AbsOperator4 & o2) {
  Operator4 op;
  for (int i=0;i<4;i++) op[i]=o1[i]-o2[i];
  return op;
}





