#include "Spacetime/WeylSpinor.h"
#include "Spacetime/LorentzTransformation.h"
#include "Spacetime/SU2Generator.h"
#include "Spacetime/SpecialOperators.h"

WeylSpinor::Right operator * (const LorentzTransformation & L, const WeylSpinor::Right & s) {
  return  L.rep1(2)*s;
}

WeylSpinor::Left operator * (const LorentzTransformation & L, const WeylSpinor::Left & s) {
  return  L.rep2(2)*s;
}
// Return the Four-momentum of this Weyl Spinor
FourVector fourMomentum(const WeylSpinor::Left  &s) {
  using namespace SpecialOperators;
  return FourVector(
		     (s.adjoint()*sigma0*s)(0,0).real(),
		    -(s.adjoint()*sigma1*s)(0,0).real(),
		    -(s.adjoint()*sigma2*s)(0,0).real(),
		    -(s.adjoint()*sigma3*s)(0,0).real());
  
}
FourVector fourMomentum(const WeylSpinor::Right &s) {
  using namespace SpecialOperators;
  return FourVector(
		     (s.adjoint()*sigma0*s)(0,0).real(),
		     (s.adjoint()*sigma1*s)(0,0).real(),
		     (s.adjoint()*sigma2*s)(0,0).real(),
		     (s.adjoint()*sigma3*s)(0,0).real());
}

// Return the Spin (
ThreeVector spin(const WeylSpinor::Left  &s) {
  double E= (s.adjoint()*Eigen::Matrix2cd::Identity()*s)(0,0).real();
  return ThreeVector((s.adjoint()*SU2Generator::JX(2)*s)(0,0).real()/E,
		     (s.adjoint()*SU2Generator::JY(2)*s)(0,0).real()/E,
		     (s.adjoint()*SU2Generator::JZ(2)*s)(0,0).real()/E);
  
}

ThreeVector spin(const WeylSpinor::Right &s) {
  double E= (s.adjoint()*Eigen::Matrix2cd::Identity()*s)(0,0).real();
  return ThreeVector((s.adjoint()*SU2Generator::JX(2)*s)(0,0).real()/E,
		     (s.adjoint()*SU2Generator::JY(2)*s)(0,0).real()/E,
		     (s.adjoint()*SU2Generator::JZ(2)*s)(0,0).real()/E);
  
}
